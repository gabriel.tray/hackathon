@extends('layout_app.base')

@section('css')
    <style>
    *{
        font-family: 'Montserrat', sans-serif;
    }
    body{
        padding: 0;
        margin: 0;
        background: url("/background.png");
        background-size: 100%;
    }
    header{
        text-align: center;
        display: flex;
        align-items: center;
        justify-content: center;
        margin-top: 200px;
        margin-bottom: 10px;
    }
    .subtexto{
        width: 80%;
        font-size: 15pt;
        color: white;
    }
    .btn{
        background: white;
        border-radius: 40px;
        width: 80%;
        height: 60px;
        margin: 0px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .corpo{
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        background: red;
    }
    .final{
        position: fixed;
        color: white;
        bottom: 0;
        margin-bottom: 15px;
        text-align: center;
        width: 100%;
    }
    .back{
        margin: 15px;
    }
    a{
        text-decoration: none;
        color: black;
    }
    </style>
@stop

@section('conteudo')
    <header>
        <div class="subtexto">
            <p><b>Sua compra foi realizada!</b></p>

            <p>Obrigado por utilizar nosso sistema.
                O código do seu pedido é {{ $pedido->id }}, totalizando R$ {{ $pedido->valor_total }}
            </p>
        </div>
    </header>

    <a href="/app"><div class="corpo">
            <div class="btn">VOLTAR PARA O MENU</div>
        </div></a>





@stop

@section('js')

@stop
