@extends('layout_app.base')

@section('css')
    <style>
    * {
        font-family: 'Montserrat', sans-serif;
    }

    body {
        padding: 0;
        margin: 0;
        background: url("/background.png");
        background-size: 100%;
    }

    header {
        background: ;
        width: 100%;
        margin-top: 150px;
    }

    .logo {
        background: ;
        width: 100%;
        height: 40px;
        display: flex;
        justify-content: center;
        align-items: center;
        position: relative;
        color: white;
    }

    .subtexto {
        background: ;
        text-align: center;
        width: 100%;
        height: 30px;
        color: white;
    }

    .corpo {
        background: ;
        height: 60%;
        width: 100%;
        position: absolute;
    }

    .btn_compra {
        background: white;
        width: 80%;
        height: 50px;
        border-radius: 40px;
        display: flex;
        justify-content: center;
        align-items: center;

    }

    .botao1 {
        display: flex;
        align-items: center;
        justify-content: center;
        margin-top: 150px;
        font-weight: 1000;
    }

    .final {
        position: fixed;
        bottom: 0;
        margin: 0px 0px 15px 0;
        text-align: center;
        width: 100%;
        color: white;
    }

    a {
        text-decoration: none;
        color: black;
    }
    </style>
@stop

@section('conteudo')
    <header>
        <div class="logo">
            <h1>SupportShop</h1>
        </div>
        <div class="subtexto">
            <p>Pronto para viver uma nova experiência em compras offline?</p>
        </div>
    </header>
    <div class="corpo">
        <div class="botao1">
            <div class="btn_compra">
                <a href="/app/register"><h4>INICIAR COMPRA</h4></a>
            </div>
        </div>
    </div>

@stop

@section('js')

@stop
