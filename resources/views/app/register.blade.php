@extends('layout_app.base')

@section('css')
    <style>
    *{
        font-family: 'Montserrat', sans-serif;
    }
    body{
        padding: 0;
        margin: 0;
        background: url("/background.png");
        background-size: 100%;
    }
    header{
        text-align: center;
        display: flex;
        align-items: center;
        justify-content: center;
        margin-top: 30px;
        margin-bottom: 30px;
    }
    .subtexto{
        width: 80%;
        font-size: 15pt;
        color: white;
    }
    .btn{
        background: rgb(3, 165, 3);
        border-radius: 40px;
        width: 80%;
        height: 60px;
        margin: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        color: white;
    }
    .corpo{
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        margin-top: 20px;
    }
    .final{
        position: fixed;
        color: white;
        bottom: 0;
        margin-bottom: 15px;
        text-align: center;
        width: 100%;
    }
    .back{
        margin: 15px;
    }
    .form{
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    input{
        height: 40px;
        width: 300px;
        font-size: 13pt;
        border-radius: 20px;
        text-align: center;
    }
    </style>
@stop

@section('conteudo')
    <div class="back">
        <img src="/back.png" alt="">
    </div>
    <header>
        <div class="subtexto">
            <p>Preencha o formulário</p>
        </div>
    </header>

    <form id="form-login" class="form-horizontal" role="form" method="POST" action="/cliente">
        <div class="form">
            <p class="nome">
                <input type="text" id="nomeid" placeholder="Nome" required="required" name="name"/>
            </p>
        </div>
        <div class="form">
            <p class="nome">
                <input type="text" id="nomeid" placeholder="Email" required="required" name="email"/>
            </p>
        </div>
        <div class="form">
            <p class="nome">
                <input type="text" placeholder="Rua" required="required" name="rua"/>
            </p>
        </div>
        <div class="form">
            <p class="nome">
                <input type="text" id="nomeid" placeholder="Bairro" required="required" name="bairro"/>
            </p>
        </div>
        <div class="form">
            <p class="nome">
                <input type="text" id="nomeid" placeholder="CEP" required="required" name="cep"/>
            </p>
        </div>
        <div class="form">
            <p class="nome">
                <input type="text" id="telfone" placeholder="Telefone" required="required" name="telefone"/>
            </p>
        </div>

        <div class="corpo">

            <button class="btn" type="submit">
                <h4>CONTINUAR</h4>
            </button>
        </div>

    </form>


@stop

@section('js')

@stop
