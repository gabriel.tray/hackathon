@extends('layout_app.base')

@section('css')
    <style>
    *{
        font-family: 'Montserrat', sans-serif;
    }
    body{
        padding: 0;
        margin: 0;
        background: url("/background.png");
        background-size: 100%;
    }
    header{
        text-align: center;
        display: flex;
        align-items: center;
        justify-content: center;
        margin-top: 50px;
        margin-bottom: 30px;
    }
    .subtexto{
        width: 80%;
        font-size: 15pt;
        color: white;
    }
    .btn{
        background: rgb(3, 165, 3);
        border-radius: 40px;
        width: 80%;
        height: 60px;
        margin: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        color: white;
    }
    .corpo{
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        margin-top: 20px;
    }
    .final{
        position: fixed;
        color: white;
        bottom: 0;
        margin-bottom: 15px;
        text-align: center;
        width: 100%;
    }
    .back{
        margin: 15px;
    }
    .d-flex {
        display: flex;
        justify-content:center;
    }
    .form{
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    input{
        height: 40px;
        width: 300px;
        font-size: 13pt;
        border-radius: 20px;
        text-align: center;
    }

    </style>
@stop

@section('conteudo')

    <div class="back">
        <img src="/back.png" alt="">
    </div>
    <header>
        <div class="subtexto"><p>Preencha o formulário</p></div> 
    </header>
    <form method="POST" action="/pedidos/create" class="text-center">
        <p class="nome">
            <input type="text" id="nomeid" placeholder="Nome Completo" required="required" name="card[card_name]"/>
        </p>

        <p class="nome">
            <input type="text" id="nomeid" placeholder="CPF" required="required" name="cpf"/>
        </p>

        <p class="nome">
            <input type="text" id="nomeid" placeholder="Número do cartão" required="required" name="card[card_number]"/>
        </p>

        <p class="nome">
            <input type="text" id="nomeid" placeholder="Código de segurança" required="required" name="card[cvv]"/>
        </p>

        <p class="nome">
            <input type="text" placeholder="Mês de vencimento" required="required" name="card[card_month]">
        </p>

        <p class="nome">
            <input type="text" placeholder="Ano de vencimento" required="required" name="card[card_year]">
        </p>
        <div class="col-md-12 col-xs-12 d-flex">
            <button type="submit" class="btn"><h4>CONTINUAR</h4></button>
        </div>
    </form>

@stop

@section('js')

@stop
</div>
