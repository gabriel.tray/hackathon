@extends('layout_app.base')

@section('css')
    <style>
    *{
        font-family: 'Montserrat', sans-serif;
    }
    body{
        padding: 0;
        margin: 0;
        background: url("/background.png");
        background-size: 100%;
    }
    header{
        text-align: center;
        display: flex;
        align-items: center;
        justify-content: center;
        margin-top: 90px;
        margin-bottom: 30px;
    }
    .subtexto{
        width: 80%;
        font-size: 15pt;
        color: white;
    }
    .btn{
        background: white;
        border-radius: 40px;
        width: 80%;
        height: 60px;
        margin: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .corpo{
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .final{
        position: fixed;
        color: white;
        bottom: 0;
        margin-bottom: 15px;
        text-align: center;
        width: 100%;
    }
    .back{
        margin: 15px;
    }
    </style>
@stop

@section('conteudo')
    <div class="back">
        <img src="/back.png" alt="">
    </div>
    <header>
        <div class="subtexto">
            <p>Como deseja receber sua compra?</p>
        </div>
    </header>
    <form action="/carrinho/entregar" method="POST">
        <input type="hidden" name="recieve" value="home">
        <div class="corpo">
            <button type="submit" class="btn"><i class="fa fa-shopping-cart fa-2x"></i>&nbsp;&nbsp;<h4>RETIRAR NA LOJA</h4></a>
        </div>
    </form>
    <form action="/carrinho/entregar" method="POST">
        <input type="hidden" name="recieve" value="local">
        <div class="corpo">
            <button type="submit" class="btn"><i class="fa fa-truck fa-2x"></i>&nbsp;&nbsp;<h4>RECEBER EM CASA</h4></a>
        </div>
    </form>

@stop

@section('js')

@stop
