@extends('layout_app.base')

@section('css')
    <style>
    *{
        font-family: 'Montserrat', sans-serif;
    }
    body{
        padding: 0;
        margin: 0;
        background: url("/background.png");
        background-size: 100%;
    }
    header{
        text-align: center;
        display: flex;
        align-items: center;
        justify-content: center;
        margin-top: 90px;
        margin-bottom: 30px;
    }
    .subtexto{
        width: 80%;
        font-size: 15pt;
        color: white;
    }
    .btn{
        background: white;
        border-radius: 40px;
        width: 80%;
        height: 60px;
        margin: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .corpo{
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .final{
        position: fixed;
        color: white;
        bottom: 0;
        margin-bottom: 15px;
        text-align: center;
        width: 100%;
    }
    .back{
        margin: 15px;
    }
    a{
        text-decoration: none;
        color: black;
    }
    </style>
@stop

@section('conteudo')
    <div class="back">
        <img src="/back.png" alt="">
    </div>
    <header>
        <div class="subtexto">
            <p>Forma de pagamento</p>
        </div>
    </header>

    <div class="corpo">
        <a href="/app/obrigado" class="btn"><i class="fa fa-money fa-2x"></i>&nbsp;&nbsp;DINHEIRO</a>
    </div>
    <div class="corpo">
        <a href="/app/obrigado" class="btn"><i class="fa fa-credit-card fa-2x"></i>&nbsp;&nbsp;DÉBITO</a>
    </div>
    <div class="corpo">
        <a href="/app/form_payment" class="btn"><i class="fa fa-credit-card fa-2x"></i>&nbsp;&nbsp;CRÉDITO</a>
    </div>


@stop

@section('js')

@stop
