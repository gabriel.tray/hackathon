@extends('layout_app.base')

@section('css')
    <style>
    body {
        padding: 0;
        margin: 0;
        background: url("/background.png");
        background-size: 100%;
    }

    * {
        font-family: 'Montserrat', sans-serif;
    }

    .bloco {
        width: 100%;
        height: 120px;
        background: rgb(255, 255, 255);
        margin: 0 10px 10px 0;
        border-bottom: 1px solid rgb(226, 226, 226);
    }

    .produto {
        background: ;
        width: 30%;
        height: 120px;
        float: left;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .nome_produto {
        background: ;
        width: 50%;
        display: flex;
        justify-content: flex-end;
    }

    .produto_name {
        background: ;
        text-align:;
        width: 90%;
    }

    .preco_produto {
        background: ;
        width: 50%;
        display: flex; <img src="pegarnaloja.png" alt="">
        justify-content: flex-start;
    }

    .produto_preco {
        background: ;
        text-align: cnter;
        width: 90%;
        margin-left: 10%;
        font-weight: bold;
    }

    .mais {
        float: right;
        position: relative;
        top: 45px;
        left: -37px;
        width: 20px;
        height: 20px;
    }

    .espaco {
        margin-top: 80px;
        height: 75px;
        display: flex;
        justify-content: center;
        align-items: center;

    }

    header {
        width: 100%;
        height: 65px;
        background: #e24141;
        display: flex;
        justify-content: center;
        align-items: center;
        position: fixed;
        top: 0;
        z-index: 99;
        box-shadow: 3px 3px 8px 3px rgba(0, 0, 0, 0.24);
    }

    .carrinho {
        background: ;
        width: 80%;
        height: 100%;
        display: flex;
        justify-content: flex-end;
        align-items: center;
    }

    nav {
        width: 90%;
        height: 80%;
        background: ;
        display: flex;
        align-items: center;
    }

    .menu {
        background: ;
        height: 100%;
        width: 20%;
        display: flex;
        align-items: center;
        justify-content: flex-start;
    }

    .btn_fechar {
        width: 80%;
        height: 60px;
        text-align: center;
        background: ;
        display: flex;
        justify-content: center;
        align-items: center;
        color: white;
        background: green;
        border-radius: 40px;
        z-index: 999;
    }

    .btn_fechar img {
        margin-left: 10px;
    }

    .footer {

        width: 100%;
        height: 100px;
        position: fixed;
        bottom: 0;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .busca {
        width: 90%;
        background: white;
        border-radius: 40px;
        display: flex;
        justify-content: center;
        align-items: center;
        box-shadow: 0px 0px 5px 2px rgba(0, 0, 0, 0.096);
    }

    .busca input {
        width: 80%;
        height: 30px;
        font-size: 15px;
        border-radius: 40px;
        border: 0px;
    }

    .busca .icon img {
        margin: 5px 10px 5px 10px;
    }
    </style>
@stop

@section('conteudo')

    <header>
        <nav>
            <div class="menu">
                <a href="/app/search"><img src="/menu.png" alt="" width="40px" height="40px"></a>
            </div>
            <div class="carrinho">
                <div class="img_carrinho">
                    <a href="/app/carrinho"><img src="/carrinho.png" alt="" width="40px" height="40px"></a>
                </div>
            </div>
        </nav>
    </header>

    <div class="espaco">
        <form id="filtro">
            <div class="busca">
                <div class="icon">
                    <img src="/search.png" alt="" width="35px" height="35px">
                </div>
                <input type="text" name="nome">
            </div>
        </form>
    </div>

    @foreach ($produtos as $produto)
        <div class="bloco">
            <div class="produto">
                <img src="{{$produto->avatar}}" alt="" width="100px" height="100px">
            </div>
            <div class="mais">
                <img src="/add.png" alt="" width="35px" height="35px" data-id-produto="{{$produto->id}}" data-id-cliente="{{$id_cliente}}" id="addItem">
            </div>
            <div class="nome_produto">
                <div class="produto_name">
                    <p>{{$produto->nome}}</p>
                </div>
            </div>

            <div class="nome_produto">
                <div class="produto_name">
                    <p>{{$produto->descricao}}</p>
                </div>
            </div>
            <div class="preco_produto">
                <div class="produto_preco">
                    <p>R${{$produto->preco}}</p>
                </div>
            </div>
        </div>
    @endforeach

    <div class="footer">
        <a href="/app/carrinho" class="btn_fechar" id="fecharCarrinho">
            <H4>FECHAR CARRINHO</H4>
            <img src="/mail-send.png" alt="" width="35px" height="35px">
        </a>
    </div>
@stop

@section('js')

<script>
    $(document).ready(function () {
      $("#addItem").click(function (e) {
        e.preventDefault();
        let data = {
          "quantidade": 1,
          "id_produto": $(this).data('id-produto'),
          "id_cliente": $(this).data('id-cliente')
        };

        $.ajax({
          url: '/carrinho/add',
          dataType : 'json',
          data: data,
          type: 'post',
          error: function (error) {
            e(error);
          },
          success: function (data) {
            genericIzitoastMenssage.showSuccessMenssage("Adicionado ao carrinho");
            s(data);
          }
        });
      });

    });
</script>
@stop
