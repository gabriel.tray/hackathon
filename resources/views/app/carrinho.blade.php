@extends('layout_app.base')

@section('css')
    <style>
    * {
        font-family: 'Montserrat', sans-serif;
    }

    body {
        padding: 0;
        margin: 0;
        background: url("/background.png");
        background-size: 100%;
    }

    .bloco {
        width: 100%;
        height: 120px;
        background: rgb(255, 255, 255);
        margin: 0 10px 10px 0;
        border-bottom: 1px solid rgb(226, 226, 226);
    }

    .produto {
        background: ;
        width: 30%;
        height: 120px;
        float: left;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .nome_produto {
        background: ;
        width: 50%;
        display: flex;
        justify-content: flex-end;
    }

    .produto_name {
        background: ;
        text-align:;
        width: 90%;
    }

    .preco_produto {
        background: ;
        width: 50%;
        display: flex;
        justify-content: flex-start;
    }

    .produto_preco {
        background: ;
        text-align: cnter;
        width: 90%;
        margin-left: 10%;
        font-weight: bold;
    }

    .mais {
        float: right;
        position: relative;
        top: 45px;
        left: -37px;
        width: 20px;
        height: 20px;
    }

    .espaco {
        height: 90px;
    }

    header {
        width: 100%;
        height: 80px;
        background: rgb(226, 65, 65);
        display: flex;
        justify-content: center;
        align-items: center;
        position: fixed;
        top: 0;
        z-index: 99;
        box-shadow: 3px 3px 8px 3px rgba(0, 0, 0, 0.24);
    }

    .carrinho {
        background: ;
        width: 80%;
        height: 100%;
        display: flex;
        justify-content: flex-end;
        align-items: center;
    }

    nav {
        width: 90%;
        height: 80%;
        background: ;
        display: flex;
        align-items: center;
    }

    .menu {
        background: ;
        height: 100%;
        width: 20%;
        display: flex;
        align-items: center;
        justify-content: flex-start;
    }

    .btn_fechar {
        width: 80%;
        height: 60px;
        text-align: center;
        background: ;
        display: flex;
        justify-content: center;
        align-items: center;
        color: white;
        background: green;
        border-radius: 40px;
        z-index: 999;
    }

    .btn_fechar img {
        margin-left: 10px;
    }

    .footer {

        width: 100%;
        height: 100px;
        position: fixed;
        bottom: 0;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .alert.alert-primary {
        background-color: black;
    }
    </style>
@stop

@section('conteudo')


    <header>
        <nav>
            <div class="menu">
                <a href="/app/search"><img src="/menu.png" alt="" width="40px" height="40px"></a>
            </div>
            <div class="carrinho">
                <div class="img_carrinho">
                    <a href="/app/carrinho"><img src="/carrinho.png" alt="" width="40px" height="40px"></a>
                </div>
            </div>
        </nav>
    </header>
    <div class="espaco"></div>
    @if(!$carrinho)
        <div class="alert alert-primary" role="alert">
            Carrinho vazio
        </div>
    @else
        @foreach ($carrinho->items() as $item)
            <div class="bloco">
                <div class="produto">
                    <img src="{{$item->produto()->avatar}}" alt="" width="100px" height="100px">
                </div>
                <div class="mais">
                    <img src="/menus.png" alt="" width="35px" height="35px" data-id-item="{{$item->id}}" id="removeItem">
                </div>
                <div class="nome_produto">
                    <div class="produto_name">
                        <p>{{$item->produto()->nome}}</p>
                    </div>
                </div>

                <div class="nome_produto">
                    <div class="produto_name">
                        <p>{{$item->produto()->descricao}}</p>
                    </div>
                </div>
                <div class="preco_produto">
                    <div class="produto_preco">
                        <p>R${{$item->produto()->preco}}</p>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="alert alert-primary" role="alert" style="color: white;">
            Total R${{$carrinho->valor_total}}
        </div>
    @endif
    <div class="footer">
        <a href="/app/recieve" class="btn_fechar">
            <H4>FECHAR CARRINHO</H4>
            <img src="/mail-send.png" alt="" width="35px" height="35px">
        </a>
    </div>
@stop

@section('js')
    <script>
    $(document).ready(function () {
      $("#removeItem").click(function (e) {
        e.preventDefault();
        let dataId = $(this).data('id-item');

        $.ajax({
          url: '/carrinho/remove/' + dataId,
          dataType: 'json',
          type: 'delete',
          error: function (error) {
            e(error);
          },
          success: function (data) {
            genericIzitoastMenssage.showSuccessMenssage("Removido do carrinho");
            s(data);
          }
        });
      });

    });
    </script>
@stop
