@extends('layout.principal')

@section('conteudo')
<div class="">
	<div class="row top_tiles">
		<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="tile-stats">
				<div class="icon"><i class="fa fa-list"></i></div>
				<div class="count">{{ $total_pedidos }}</div>
				<h3>Número de pedidos</h3>
				<p>Total de pedidos realizados</p>
			</div>
		</div>
	</div>
</div>
@stop
