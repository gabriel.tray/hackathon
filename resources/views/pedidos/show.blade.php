@extends('layout.principal')

@section('conteudo')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Detalhes do pedido Nº {{ $pedido->id }}</h3>
            </div>
        </div>

        <div class="clearfix"></div>
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-md-12">
                <div class="x_papencilnel">
                    <div class="x_content">
                        <table id="tablea-itens-pedido" class="table table-striped table-responsive">
                            <thead>
                            <tr>
                                <th>Nome do produto</th>
                                <th>Quantidade</th>
                                <th>Valor</th>
                                <td></td>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach ($pedido->itensPedido as $itemPedido)
                                <tr>
                                    <td>{{ $itemPedido->produto->nome }}</td>
                                    <td>{{ $itemPedido->quantidade }}</td>
                                    <td>{{ $itemPedido->valor }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <a href="{{action('PedidosController@index')}}"><i class="btn btn-sm btn-success fa fa-arrow-circle-left editar">&nbsp;&nbsp;Voltar</i></a>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="/js/contatos/contato-modulo.js"></script>
    <script src="/js/contatos/contato-form-busca.js"></script>
@stop
