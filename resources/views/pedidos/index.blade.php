@extends('layout.principal')

@section('conteudo')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Pedidos</h3>
            </div>
        </div>

        <div class="clearfix"></div>
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Lista de pedidos</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="table_motoboys" class="table table-striped table-responsive">
                            <thead>

                            <tr>
                                <th>Nº do pedido</th>
                                <th>Nome do cliente</th>
                                <th>Valor do pedido</th>
                                <th>Status</th>
                                <th>Montador</th>
                                <th>Tipo do pagamento</th>
                                <td></td>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach ($pedidos as $pedido)
                                <tr>
                                    <td>{{ $pedido->id }}</td>
                                    <td>{{ $pedido->cliente->name }}</td>
                                    <td>{{ $pedido->valor_total }}</td>
                                    <td>{{ $pedido->status() }}</td>
                                    <td>{{ $pedido->montador }}</td>
                                    <td>{{ $pedido->tipoPagamento() }}</td>
                                    <td><a href="{{action('PedidosController@show', $pedido->id)}}">
                                           <i data-id-contato="{{ $pedido->id }}"
                                              class="btn btn-sm btn-info fa fa-eye editar"></i>
                                       </a></td>
                                    <td><a href="{{ action('PedidosController@manualPay', $pedido->id) }}">
                                    <i class="btn btn-success fa fa-money">&nbsp;&nbsp;Baixa manual</i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="/js/contatos/contato-modulo.js"></script>
    <script src="/js/contatos/contato-form-busca.js"></script>
@stop
