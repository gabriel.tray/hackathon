<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Corredor extends Model {

    public $timestamps = false;

    protected $fillable = ['descricao'];

}
