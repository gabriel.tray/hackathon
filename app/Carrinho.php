<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrinho extends Model {

    public $timestamps = true;
    protected $fillable = ['valor_total', 'id_cliente', 'tipo_entrega'];

    public function cliente(){
        return $this->belongsTo('App\Cliente', 'id_cliente');
    }

    public function items(){
        return $this->hasMany(ItemCarrinho::class, 'id_carrinho')->get();
    }
}


