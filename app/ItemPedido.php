<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemPedido extends Model {

    protected $table = 'pedido_items';
    public $timestamps = false;
    protected $fillable = ['id_pedido', 'id_produto', 'quantidae', 'valor'];

    public function pedido(){
        return $this->belongsTo('App\Pedido', 'id_pedido' );
    }

    public function produto(){
        return $this->belongsTo('App\Produto', 'id_produto');
    }
}
