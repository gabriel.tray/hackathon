<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\ValueObjects\PedidoStatus;
use App\Http\ValueObjects\TipoPagamento;

class Pedido extends Model {

    public $timestamps = true;
    protected $fillable = ['valor_total', 'montador', 'id_cliente', 'tipo_pagamento', 'status', 'tipo_entrega'];

    public function cliente(){
        return $this->belongsTo('App\Cliente', 'id_cliente');
    }

    public function itensPedido()
    {
        return $this->hasMany('App\ItemPedido', 'id_pedido');
    }

    public function status ()
    {
        switch($this->status)
        {
            case PedidoStatus::AGUARNDADO_PAGAMENTO:
                return 'Aguardando pagamento';
            case PedidoStatus::AGUARDANDO_MONTEGEM:
                return 'Aguardando montagem';
            case PedidoStatus::EM_MONTAGEM:
                return 'Em montagem';
            case PedidoStatus::AGUARDANDO_ENTREGA:
                return 'Aguardando entrega';
            case PedidoStatus::AGUARDANDO_RETIRADA:
                return  'Aguardando retirada';
            case PedidoStatus::FINALIZADO:
                return 'Finalizado';
        }
    }

    public function tipoPagamento ()
    {
        switch($this->tipo_pagamento)
        {
            case TipoPagamento::DINHEIRO:
                return 'Dinheiro';
            case TipoPagamento::CARTAO_DE_CREDITO:
                return 'Cartão de crédito';
            case TipoPagamento::CARTAO_DE_DEBITO:
                return 'Cartão de débito';
            case TipoPagamento::AGUARDANDO_PAGAMENTO:
                return 'Aguardando pagamento';
        }
    }

}


