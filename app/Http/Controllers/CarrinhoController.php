<?php

namespace App\Http\Controllers;

use App\Carrinho;
use App\ItemCarrinho;
use App\Pedido;
use App\Produto;
use App\Http\ValueObjects\TipoEntrega;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CarrinhoController extends Controller
{

    public function addItem(Request $request){

        $idLogin = $request->request->get('id_cliente');
        $carrinho = $this->getOrCreateCarrinhoByLogin($idLogin);
        $produto = Produto::find($request->request->get('id_produto'));
        $quantidade = $request->request->get('quantidade');
        $carrinho->valor_total = $produto->preco * $quantidade;
        $carrinho->save();

        $item = new ItemCarrinho([
            'id_carrinho' => $carrinho->id,
            'valor' => $produto->preco,
            'quantidade' => $quantidade,
            'id_produto' => $produto->id
        ]);
        $item->save();

        return $this->getHttpOkResponse($item->toArray());
    }

    public function removeItem($id){

        $item = ItemCarrinho::find($id);
        $carrinho = $item->carrinho();
        $carrinho->saldo_total -= $item->valor * $item->quantidade;
        $item->delete();
        $carrinho->save();

        return $this->getHttpOkResponse([], Response::HTTP_NO_CONTENT);
    }


    private function getOrCreateCarrinhoByLogin($idCliente)
    {
        $carrinho = Carrinho::where('id_cliente', $idCliente)->get()->first();

        if(is_null($carrinho)){
            $carrinho =new Carrinho(['id_cliente' => $idCliente]);
            $carrinho->save();
        }
        return $carrinho;
    }

    public function selectRecieve(Request $request)
    {
        $tipoDeEntrega = $request->get('recieve');
        $cliente = session()->get('user')[0];
        $carrinho = Carrinho::where('id_cliente', $cliente['id'])->get()->first();
        if ($tipoDeEntrega === 'home') {
            $carrinho->tipo_entrega = TipoEntrega::ENTREGAR_EM_CASA;
        } else {
            $carrinho->tipo_entrega= TipoEntrega::RETIRADA_NA_LOJA;
        }
        $carrinho->save();
        return redirect('/app/payment_methods');
    }

}
