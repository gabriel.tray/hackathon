<?php

namespace App\Http\Controllers;

use App\Cliente;
use Symfony\Component\HttpFoundation\Request;

class ClienteController extends Controller
{
    public function insert(Request $request){

        $cliente = new Cliente($request->request->all());
        $cliente->save();
        session()->push("user", $cliente->toArray());

        return redirect("app/search");
    }
}
