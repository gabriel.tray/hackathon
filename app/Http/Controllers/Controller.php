<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    protected function queryParams(Builder &$builder, $key , $value){
        $builder->where($key, "LIKE", "%$value%");
    }

    protected function getHttpOkResponse($data, $responseCode = Response::HTTP_OK){
        $response = new JsonResponse();
        $response->setData($data);
        $response->setStatusCode($responseCode);
        return $response;
    }
}
