<?php

namespace App\Http\Controllers;

use App\Carrinho;
use App\Http\ValueObjects\PedidoStatus;
use App\Http\ValueObjects\TipoPagamento;
use App\Pedido;
use GuzzleHttp\Client;
use Illuminate\Support\FacadPagamentoes\Log;
use Symfony\Component\HttpFoundation\Request;

class PedidosController extends Controller
{
    public function search(Request $request){

        $query = Pedido::query();

        foreach ($request->query->all() as $key => $value){
            $this->queryParams($query, $key, $value);
        }

        return $this->getHttpOkResponse($query->get());
    }

    public function createPedido(Request $request) {
        $carrinho = $this->getCarrinhoByLogin(session()->get('user')[0]['id']);
        $tipoPagamento = $request->get('tipo_pagamento');
        $card = $request->get('card');

        $pedido = new Pedido([
            'valor_total' => $carrinho->valor_total,
            'id_cliente' => $carrinho->id_cliente,
            'tipo_entrega' => $carrinho->tipo_entrega,
            'tipo_pagamento' => TipoPagamento::CARTAO_DE_CREDITO,
            'status' => TipoPagamento::AGUARDANDO_PAGAMENTO
        ]);

        if((int)$tipoPagamento === TipoPagamento::CARTAO_DE_CREDITO){
            if($this->sendRequestToYapay($pedido, $card)){
                $pedido->status = PedidoStatus::AGUARDANDO_MONTEGEM;
            }
        }

        $pedido->save();
        $carrinho->delete();
        return redirect('/app/obrigado');
    }

    public function finalizarPedido($id){
        $pedido = Pedido::find($id);
        $pedido->status = PedidoStatus::FINALIZADO;
        $pedido->save();

        return $this->getHttpOkResponse($pedido->toArray());
    }

    public function aprovar($id){
        $pedido = Pedido::find($id);
        $pedido->status = PedidoStatus::AGUARDANDO_MONTEGEM;
        $pedido->save();

        return $this->getHttpOkResponse($pedido->toArray());
    }

    public function index()
    {
        $pedidos = Pedido::all();
        return view ('pedidos.index')->with('pedidos', $pedidos);
    }

    public function show($id)
    {
        $pedido = Pedido::find($id);
        return view ('pedidos.show')->with('pedido', $pedido);
    }

    public function manualPay($id)
    {
        $pedido = Pedido::find($id);
        $pedido->status = PedidoStatus::AGUARDANDO_MONTEGEM;
        $pedido->save();
        return redirect('/pedidos/index');
    }

    private function sendRequestToYapay(Pedido $pedido, $card){

        $data = [
            'token_account' => '4f14b0a42dd2fe0',
            'customer' => [
                'contacts' => [ 0 => [
                    'type_contact' => 'M',
                    'number_contact' => '14999999999'
                ]],
                'addresses' => [ 0 => [
                    'type_address' => 'B',
                    'postal_code'=> '05707-001',
                    'street' => 'Rua Itapaiuna',
                    'number' => '2434',
                    'completion' => 'Perto da minha casa',
                    'neighborhood' => 'Jardim Morumbi',
                    'city' => 'Sao Paulo',
                    'state' => 'SP'
                ]
                ],
                'name' => $pedido->cliente->name,
                'birth_date' => '11/02/1999',
                'cpf' => '677.036.020-94',
                'email' => $pedido->cliente->email
            ],
            'transaction_product' => [ 0 => [
                'description' => 'Compra mensal',
                'quantity' => '1',
                'code' => '1',
                'price_unit' => $pedido->valor_total
            ]],
            'transaction' => [
                'url_notification' => 'http://1fb3d9eb.ngrok.io/payment'
            ],
            'payment' => [
                'payment_method_id' => '4',
                'card_name' => $card['card_name'],
                'card_number' => $card['card_number'],
                'card_expdate_month' => $card['card_month'],
                'card_expdate_year' => $card['card_year'],
                'card_cvv' => $card['cvv'],
                'split' => '1'
            ]
        ];

        $client = new Client();
        try{

        $response = $client->post(
            'https://api.intermediador.sandbox.yapay.com.br/api/v3/transactions/payment', [\GuzzleHttp\RequestOptions::JSON => $data]);
        }catch(\Exception $ex){

        }
        $statusCode = $response->getStatusCode();
        $responseBody = $response->getBody();
        return $statusCode === 200;
    }

    private function getCarrinhoByLogin($idCliente)
    {
        return Carrinho::where('id_cliente', $idCliente)->get()->first();
    }

}
