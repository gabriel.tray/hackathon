<?php namespace App\Http\Controllers;

use App\Carrinho;
use App\Pedido;
use App\ItemCarrinho;
use App\Produto;
use Symfony\Component\HttpFoundation\Request;

class AppController extends Controller {

	public function home()
	{
		return view('app.welcome');
	}

    public function register()
    {
        if(session()->get('user')){
            return redirect('/app/search');
        }
        return view('app.register');
    }


    public function carrinho()
    {
        return view('app.carrinho')->with(['carrinho' => $this->searchCarrinho()]);
    }

    public function obrigado()
    {
        $user = session()->get('user')[0];
        $pedidos = Pedido::where('id_cliente', $user['id'])->get();
        return view('app.obrigado')->with(['pedido' => $pedidos->last()]);
    }

    public function payment_methods()
    {
        return view('app.payment_methods');
    }

    public function search(Request $request)
    {
        $user = session()->get('user')[0];
        $data = [
          'produtos' => $this->searchProdutos($request),
          'id_cliente'=> $user['id']
        ];
        return view('app.search')->with($data);
    }

    public function recieve_locale()
    {
        return view('app.recieve_locale');
    }

    public function searchCarrinho(){

        $carrinho = Carrinho::where('id_cliente', session()->get('user')[0]['id'])->get()->first();

        return $carrinho;
    }

    private function searchProdutos(Request $request){

        $query = Produto::query();

        foreach ($request->query->all() as $key => $value){
            $this->queryParams($query, $key, $value);
        }

        return $query->get();
    }

    public function form_payment()
    {
        return view('app.form_payment');
    }

}
