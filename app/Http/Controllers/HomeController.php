<?php namespace App\Http\Controllers;

use App\Contato;
use App\ContatoTelefone;
use App\Pedido;
use App\TipoTelefone;
use App\User;

class HomeController extends Controller {

	public function home()
	{
        $params =  [
            "total_pedidos" => count(Pedido::all()),
        ];

		return view('home')->with($params);
	}

}
