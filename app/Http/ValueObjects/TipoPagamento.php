<?php namespace App\Http\ValueObjects;

class TipoPagamento 
{
    const DINHEIRO = 1;
    const CARTAO_DE_CREDITO = 2;
    const CARTAO_DE_DEBITO = 3;
    const AGUARDANDO_PAGAMENTO = null;
}