<?php namespace App\Http\ValueObjects;

class PedidoStatus 
{
    const AGUARNDADO_PAGAMENTO = 1;
    const AGUARDANDO_MONTEGEM = 2;
    const EM_MONTAGEM = 3;
    const AGUARDANDO_ENTREGA = 4;
    const AGUARDANDO_RETIRADA = 5;
    const FINALIZADO = 6;
}