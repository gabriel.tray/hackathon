<?php namespace App\Http\ValueObjects;

class TipoEntrega 
{
    const RETIRADA_NA_LOJA = 1;
    const ENTREGAR_EM_CASA = 2;
}