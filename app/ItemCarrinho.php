<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCarrinho extends Model {

    protected $table = 'carrinho_items';
    public $timestamps = false;
    protected $fillable = ['id_carrinho', 'id_produto', 'quantidade', 'valor'];

    public function carrinho(){
        return $this->belongsTo('App\Carrinho', 'id_carrinho' )->get()->first();
    }

    public function produto(){
        return $this->belongsTo('App\Produto', 'id_produto')->get()->first();
    }
}
