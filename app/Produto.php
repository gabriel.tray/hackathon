<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model {

    protected $fillable = ['nome', 'descricao', 'avatar', 'preco', 'id_setor','id_corredor'];

    public function corredor(){
        return $this->belongsTo('App\Corredor', 'id_corredor' )->get()->first();
    }

    public function setor(){
        return $this->belongsTo('App\Corredor', 'id_setor' )->get()->first();
    }

}
