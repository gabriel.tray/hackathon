<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string("nome");
            $table->string("descricao");
            $table->string("avatar");
            $table->decimal("preco");
            $table->integer("id_setor")->unsigned();
            $table->integer('id_corredor')->unsigned();

            $table->foreign("id_setor")
                ->references('id')->on('sectors')
                ->onDelete('cascade');

            $table->foreign('id_corredor')
                ->references('id')->on('corredors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('produtos');
    }

}
