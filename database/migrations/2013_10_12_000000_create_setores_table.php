<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetoresTable extends Migration {

    public function up()
    {
        Schema::create('sectors', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->string("descricao");
        });
    }

    public function down()
    {
        Schema::drop('sectors');
    }

}
