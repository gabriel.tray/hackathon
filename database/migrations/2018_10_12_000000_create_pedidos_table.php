<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string("montador");
            $table->integer("status");
            $table->integer("tipo_pagamento");
            $table->integer("tipo_entrega");
            $table->decimal("valor_total");
            $table->integer("id_cliente")->unsigned();
            $table->timestamps();

            $table->foreign("id_cliente")
                ->references('id')->on('clientes')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pedidos');
    }

}
