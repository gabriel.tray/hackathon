<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarrinhoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carrinhos', function(Blueprint $table)
        {
            $table->increments('id');
            $table->decimal("valor_total");
            $table->integer("id_cliente")->unsigned();
            $table->integer("tipo_entrega");
            $table->timestamps();

            $table->foreign("id_cliente")
                ->references('id')->on('clientes')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('carrinhos');
    }

}
