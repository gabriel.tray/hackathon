<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarrinhoItemsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carrinho_items', function(Blueprint $table)
        {
            $table->increments('id');
            $table->decimal("valor");
            $table->integer("quantidade");
            $table->integer("id_carrinho")->unsigned();
            $table->integer('id_produto')->unsigned();

            $table->foreign("id_carrinho")
                ->references('id')->on('carrinhos')
                ->onDelete('cascade');

            $table->foreign('id_produto')
                ->references('id')->on('produtos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('carrinho_items');
    }

}
