<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidoItemsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_items', function(Blueprint $table)
        {
            $table->increments('id');
            $table->decimal("valor");
            $table->integer("quantidade");
            $table->integer("id_pedido")->unsigned();
            $table->integer('id_produto')->unsigned();

            $table->foreign("id_pedido")
                ->references('id')->on('pedidos')
                ->onDelete('cascade');

            $table->foreign('id_produto')
                ->references('id')->on('produtos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pedido_items');
    }

}
