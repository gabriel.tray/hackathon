<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorredorTable extends Migration {

    public function up()
    {
        Schema::create('corredors', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->string("descricao");
        });
    }

    public function down()
    {
        Schema::drop('corredors');
    }

}
