<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();


Route::get('/', "HomeController@home");
Route::get('/home', "HomeController@home");

Route::get('/pedidos/index', "PedidosController@index");
Route::get('/pedidos/{id}/show', 'PedidosController@show');
Route::get('/pedidos{id}/manualPay', 'PedidosController@manualPay');
Route::post('/pedidos/create', 'PedidosController@createPedido');
Route::get('/produtos', 'ProdutosController@search');
Route::get('/carrinho/{cliente_id}', 'CarrinhoController@search');
Route::post('/pedidos/{id}/aprovar', 'PedidosController@aprovar');
Route::post('/pedidos/{id}/finalizar', 'PedidosController@finalizarPedido');
Route::post('/carrinho/add', 'CarrinhoController@addItem');
Route::delete('/carrinho/remove/{id}', 'CarrinhoController@removeItem');
Route::post('/carrinho/entregar', 'CarrinhoController@selectRecieve');
Route::post('/cliente', 'ClienteController@insert');

Route::get('/app', "AppController@home");
Route::get('/app/register', "AppController@register");
Route::get('/app/carrinho', "AppController@carrinho");
Route::get('/app/obrigado', "AppController@obrigado");
Route::get('/app/payment_methods', "AppController@payment_methods")->name('payment');
Route::get('/app/form_payment', "AppController@form_payment");
Route::get('/app/recieve', "AppController@recieve_locale");
Route::get('/app/search', "AppController@search");
